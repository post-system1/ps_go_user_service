package service

import (
	"context"
	"post_system/ps_go_user_service/config"
	"post_system/ps_go_user_service/genproto/user_service"
	"post_system/ps_go_user_service/grpc/client"
	"post_system/ps_go_user_service/models"
	"post_system/ps_go_user_service/pkg/logger"
	"post_system/ps_go_user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BranchService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedBranchServiceServer
}

func NewBranchService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *BranchService {
	return &BranchService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *BranchService) Create(ctx context.Context, req *user_service.CreateBranchRequest) (resp *user_service.Branch, err error) {

	i.log.Info("---CreateBranch------>", logger.Any("req", req))

	pKey, err := i.strg.Branch().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateBranch->Branch->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Branch().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyBranch->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BranchService) GetByID(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error) {

	i.log.Info("---GetBranchByID------>", logger.Any("req", req))

	resp, err = i.strg.Branch().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBranchByID->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BranchService) GetList(ctx context.Context, req *user_service.GetListBranchRequest) (resp *user_service.GetListBranchResponse, err error) {

	i.log.Info("---GetBranchs------>", logger.Any("req", req))

	resp, err = i.strg.Branch().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBranchs->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BranchService) Update(ctx context.Context, req *user_service.UpdateBranchRequest) (resp *user_service.Branch, err error) {

	i.log.Info("---UpdateBranch------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Branch().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateBranch--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Branch().GetByPKey(ctx, &user_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetBranch->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BranchService) Patch(ctx context.Context, req *user_service.UpdatePatchBranchRequest) (resp *user_service.Branch, err error) {

	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.Data.AsMap(),
	}

	rowsAffected, err := i.strg.Branch().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchBranch--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Branch().GetByPKey(ctx, &user_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetBranch->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BranchService) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Empty, err error) {

	i.log.Info("---DeleteBranch------>", logger.Any("req", req))

	err = i.strg.Branch().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteBranch->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &user_service.Empty{}, nil
}
