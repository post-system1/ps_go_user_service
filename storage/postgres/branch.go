package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"post_system/ps_go_user_service/genproto/user_service"
	"post_system/ps_go_user_service/models"
	"post_system/ps_go_user_service/pkg/helper"
	"post_system/ps_go_user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) storage.BranchRepoI {
	return &BranchRepo{
		db: db,
	}
}

func (c *BranchRepo) Create(ctx context.Context, req *user_service.CreateBranchRequest) (resp *user_service.BranchPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "branch" (
				id,
				name,	
				address,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.Address,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.BranchPrimaryKey{Id: id.String()}, nil
}

func (c *BranchRepo) GetByPKey(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error) {

	query := `
		SELECT
			id,
			name,	
			address,
			created_at,
			updated_at
		FROM "branch"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		address   sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&address,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Branch{
		Id:        id.String,
		Name:      name.String,
		Address:   address.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *BranchRepo) GetAll(ctx context.Context, req *user_service.GetListBranchRequest) (resp *user_service.GetListBranchResponse, err error) {

	resp = &user_service.GetListBranchResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,	
			address,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "branch"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			address   sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&address,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Branches = append(resp.Branches, &user_service.Branch{
			Id:        id.String,
			Name:      name.String,
			Address:   address.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *BranchRepo) Update(ctx context.Context, req *user_service.UpdateBranchRequest) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "branch"
			SET
				name = :name,
				address = :address,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":      req.GetId(),
		"name":    req.GetName(),
		"address": req.GetAddress(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BranchRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"branch"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *BranchRepo) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) error {

	query := `DELETE FROM "branch" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
