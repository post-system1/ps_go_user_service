package postgres

import (
	"context"
	"database/sql"
	"post_system/ps_go_user_service/genproto/user_service"
	"post_system/ps_go_user_service/pkg/helper"
	"post_system/ps_go_user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type MarketRepo struct {
	db *pgxpool.Pool
}

func NewMarketRepo(db *pgxpool.Pool) storage.MarketRepoI {
	return &MarketRepo{
		db: db,
	}
}

func (c *MarketRepo) Create(ctx context.Context, req *user_service.CreateMarket) (resp *user_service.MarketPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "market" (
				id,
				branch_id,	
				name,
				description,
				updated_at
			) VALUES ($1, $2, $3, $4, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.Name,
		req.Description,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.MarketPrimaryKey{Id: id.String()}, nil
}

func (c *MarketRepo) GetByPKey(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *user_service.Market, err error) {

	query := `
		SELECT
			id,
			branch_id,	
			name,	
			description,
			created_at,
			updated_at
		FROM "market"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branchId    sql.NullString
		name        sql.NullString
		description sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branchId,
		&name,
		&description,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Market{
		Id:          id.String,
		BranchId:    branchId.String,
		Name:        name.String,
		Description: description.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *MarketRepo) GetAll(ctx context.Context, req *user_service.GetListMarketRequest) (resp *user_service.GetListMarketResponse, err error) {

	resp = &user_service.GetListMarketResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,	
			name,	
			description,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "market"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branchId    sql.NullString
			name        sql.NullString
			description sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branchId,
			&name,
			&description,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Markets = append(resp.Markets, &user_service.Market{
			Id:          id.String,
			BranchId:    branchId.String,
			Name:        name.String,
			Description: description.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *MarketRepo) Update(ctx context.Context, req *user_service.UpdateMarket) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "market"
			SET
				branch_id = :branch_id,
				name = :name,
				description = :description,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"name":        req.GetName(),
		"description": req.GetDescription(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *MarketRepo) Delete(ctx context.Context, req *user_service.MarketPrimaryKey) error {

	query := `DELETE FROM "market" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
