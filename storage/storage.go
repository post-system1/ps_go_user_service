package storage

import (
	"context"

	"post_system/ps_go_user_service/genproto/user_service"
	"post_system/ps_go_user_service/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Branch() BranchRepoI
	Market() MarketRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUserRequest) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUserRequest) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
}

type BranchRepoI interface {
	Create(ctx context.Context, req *user_service.CreateBranchRequest) (resp *user_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error)
	GetAll(ctx context.Context, req *user_service.GetListBranchRequest) (resp *user_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateBranchRequest) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.BranchPrimaryKey) error
}

type MarketRepoI interface {
	Create(ctx context.Context, req *user_service.CreateMarket) (resp *user_service.MarketPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.MarketPrimaryKey) (resp *user_service.Market, err error)
	GetAll(ctx context.Context, req *user_service.GetListMarketRequest) (resp *user_service.GetListMarketResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateMarket) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.MarketPrimaryKey) error
}
