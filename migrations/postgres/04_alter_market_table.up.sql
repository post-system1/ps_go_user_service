

ALTER TABLE "market" ADD COLUMN  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE "market" ADD COLUMN  "updated_at" TIMESTAMP;
