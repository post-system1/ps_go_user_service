
CREATE TABLE IF NOT EXISTS "users" (
    "id" UUID PRIMARY KEY,
    "first_name" VARCHAR(50),
    "last_name" VARCHAR(50),
    "email" VARCHAR(50) UNIQUE,
    "phone_number" VARCHAR(60),
    "address" TEXT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
