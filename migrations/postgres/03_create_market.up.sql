
CREATE TABLE "market" (
    "id" UUID PRIMARY KEY,
    "branch_id" UUID NOT NULL REFERENCES "branch" ("id"),
    "name" VARCHAR(56) NOT NULL,
    "description" TEXT
);

