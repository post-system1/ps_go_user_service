
CREATE TABLE IF NOT EXISTS "branch" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(50),
    "address" TEXT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
